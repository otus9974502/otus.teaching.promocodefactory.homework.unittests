﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using AutoFixture.Kernel;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{

    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private Partner _partner;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            _partner = CreateBasePartner();
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(_partner);
        }

        // 1. Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async Task GetPartner_ShouldBe_NotFoundResult()
        {
            // Arrange
            var autoFixture = new Fixture();
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        // 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var autoFixture = new Fixture();
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partner.IsActive = false;
            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(_partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerSetLimit_ZeroNumberIssuedPromoCodes()
        {
            // Arrange
            var partner = CreateBasePartner();

            var partnerId = partner.Id;
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        //если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerSetLimit_NotZeroNumberIssuedPromoCodes()
        {
            // Arrange
            var partner = CreateBasePartner();
            var limit = partner.NumberIssuedPromoCodes;

            var partnerId = partner.Id;
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(limit);
        }

        /// При установке лимита нужно отключить предыдущий лимит
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_HasActiveLimit_ShouldSetCancelDateNow()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var activeOldLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.PartnerLimits.FirstOrDefault(f => f.Id == activeOldLimit.Id).CancelDate.Should().NotBeNull();

        }

        /// Лимит должен быть больше 0
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerSetMoreThanZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("2A0A253F-45B9-4E80-AA7F-A9EA968C9852");
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            request.Limit = -1;

            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var setLimitActionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            setLimitActionResult.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
    }
}